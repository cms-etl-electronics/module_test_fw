library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity flash_bit is
  generic
  (
    g_DATA_WIDTH : integer := 8
  );
  port
  (
    clk_i     : in std_logic;
    reset_i   : in std_logic;
    data_i    : in std_logic_vector(g_DATA_WIDTH - 1 downto 0);
    set_cnt_i : in std_logic_vector(31 downto 0);

    data_o   : out std_logic_vector(g_DATA_WIDTH - 1 downto 0);
    active_o : out std_logic

  );
end flash_bit;

architecture behavioral of flash_bit is

  signal cnt                      : std_logic_vector(3 downto 0);
  signal cnt_clk                  : std_logic_vector(13 downto 0);
  signal start                    : std_logic;
  signal active                   : std_logic;
  attribute MARK_DEBUG            : string;
  attribute MARK_DEBUG of cnt     : signal is "true";
  attribute MARK_DEBUG of cnt_clk : signal is "true";

begin

  active_o <= active;

  process (clk_i) is
  begin
    if rising_edge(clk_i) then

      if (reset_i = '1') then
        --------------------------------------------------------------------------------
        -- Reset
        --------------------------------------------------------------------------------
        active  <= '0';
        start   <= '0';
        cnt     <= (others => '0');
        cnt_clk <= (others => '0');
        data_o  <= data_i;

      else

        --------------------------------------------------------------------------------
        -- Start Count
        --------------------------------------------------------------------------------

        if (and_reduce(data_i) = '1' and start = '0') then
          start   <= '1';
          cnt_clk <= std_logic_vector(unsigned(cnt_clk) + 1);
          cnt     <= std_logic_vector(unsigned(cnt) + 1);
        end if;

        --------------------------------------------------------------------------------
        -- Increase Clock Count
        --------------------------------------------------------------------------------
        if start = '1' then
          if to_integer(unsigned(cnt_clk)) <= to_integer(unsigned(set_cnt_i)) then
            cnt_clk                          <= std_logic_vector(unsigned(cnt_clk) + 1);
            data_o                           <= data_i;
          else
            cnt_clk <= (others => '0');
          end if;
        end if;

        --------------------------------------------------------------------------------
        -- Check Flashing Bit seen
        --------------------------------------------------------------------------------
        if to_integer(unsigned(cnt_clk)) = to_integer(unsigned(set_cnt_i)) then

          if (and_reduce(data_i) = '1') then
            cnt   <= std_logic_vector(unsigned(cnt) + 1);
            start <= start;
          else
            cnt   <= (others => '0');
            start <= '0';
          end if;

        end if;

        --------------------------------------------------------------------------------
        -- Activate if flashing bit is on 5 clock cycle times in a row
        --------------------------------------------------------------------------------

        if (to_integer(unsigned(cnt)) >= 5) then
          active <= '1';
          cnt    <= std_logic_vector(to_unsigned(5, cnt'length));
        end if;

        --------------------------------------------------------------------------------
        -- Clear if flashing bit is active and correct clock cycle
        --------------------------------------------------------------------------------

        if active = '1' and to_integer(unsigned(cnt_clk)) = to_integer(unsigned(set_cnt_i)) then
          data_o <= (others => '0');
        end if;

      end if;
    end if;
  end process;
end behavioral;