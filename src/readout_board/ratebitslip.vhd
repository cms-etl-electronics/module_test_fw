library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library work;
use work.types.all;

entity ratebitslip is
  generic
  (
    NUM_ETROCS : integer := 28;
    CNT_BITS   : integer := 26;
    WIDTH      : integer := 224
  );
  port
  (
    clock_i       : in std_logic;
    uplink_data_i : in std_logic_vector (WIDTH - 1 downto 0);
    slip_i        : in integer_vector_local (NUM_ETROCS - 1 downto 0);
    rate_i        : in integer range 0 to 2; -- 0==320, 1==640, 2==1280

    uplink_data_o : out std_logic_vector (WIDTH - 1 downto 0)
  );
end ratebitslip;

architecture rtl of ratebitslip is
  type data_slip_array_t is array (integer range 2 downto 0) of std_logic_vector(WIDTH - 1 downto 0);
  signal data_slip_320_640_1280 : data_slip_array_t;

begin
  --------------------------------------------------------------------------------
  -- Generate Bitslippers for Each Data Rate
  --------------------------------------------------------------------------------

  slip_irate_gen : for IRATE in 0 to 2 generate
    constant NUM : integer := 2 ** (IRATE + 3);
  begin

    slip_ietroc_gen : for IETROC in 0 to 224/NUM - 1 generate
    begin

      bitslip_inst : entity work.bitslip
        generic
        map (
        g_DATA_WIDTH           => NUM,
        g_SLIP_CNT_WIDTH       => 5,
        g_TRANSMIT_LOW_TO_HIGH => true) -- TODO: check this
        port map
        (
          clk_i      => clock_i,
          slip_cnt_i => slip_i(IETROC),
          data_i     => uplink_data_i((IETROC + 1) * NUM - 1 downto IETROC * NUM),
          data_o     => data_slip_320_640_1280(IRATE)((IETROC + 1) * NUM - 1 downto IETROC * NUM)
        );

    end generate;
  end generate;

  -- multiplex the 3 different slippers into one signal
  process (clock_i) is
  begin
    if (rising_edge(clock_i)) then
      uplink_data_o <= data_slip_320_640_1280(rate_i);
    end if;
  end process;
end rtl;